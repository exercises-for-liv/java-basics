# Was soll denn das mit den Ordnern?

*Packages* heißt das Stichwort. Wir wollen mal nicht mehr ins Detail gehen als
nötig, aber falls du mal irgendwann denkst:

    "Hey ich strukturier das mal schöner mit Ordnern!"

und plötzlich nix mehr läuft, reden wir kurz über die Basics.
Java nutzt Packages zum strukturieren von Code. Wenn wir mal mehr Dateien
haben, werden wir noch froh drum sein! Im Moment ist es noch eher akademischer
Natur. Wir kümern uns also nur darum, was wir machen müssen, damit die Sache
trotzdem läuft!

Kompilieren der *.java*-Datei.
Entweder in den jeweiligen Ordner wechseln wo unsere Datei liegt und erneut:

    javac Hello.java

nutzen, ooooder im Wurzelverzeichnis (also eine Ebene höher, wo auch diese Markdown-Datei liegt, die du
gerade liest):

    javac .\dummy\inner\Hello.java

(unter Windows, denk dran unter Linux die Slashes zu drehen).
Wenn du jetzt im Wurzelverzeichnis

    java Hello

aufrufst, wirst du keinen Spass haben! (Try it!)

Wir sehen einen Fehler, der schonmal in die richtige Richtung deutet!

**Takeaway: lies Fehlermeldungen! Es braucht bisschen Übung, aber die sind oft hilfreich, wenn man sich auf sie einlässt!**

Ok...die Klasse wird nicht gefunden. Wir denken uns also:

    "Aio...logo...ich bin ja auch im falschen Ordner..."

Wir wechseln also in den richtigen Ordner und bekommen...wieder ne Fehlermeldung...
eine leicht andere...aber immer noch Fehler!!!
Shit!!! Was soll das denn?!

Ok, ich gebe zu hier wird's bisschen komisch, aber bleib bei mir!

Diese Ordner sind nicht einfach nur Ordner!
Die sind eine Repräsentation unserer *Packagestruktur*!
Schau mal in *Hello.java*! Das ist nicht identisch zu der Variante in Lektion 01-01!
In der ersten Zeile steht eine Package-Angabe!
Und zwar *dummy.inner*!
Dass das mit den Ordner korrespondiert ist kein Zufall, sondern nötig!
Das was in den *.java* Dateien als package angegeben ist, muss im Dateisystem durch eine
entsprechende Ordnerstruktur repräsentiert werden! Sonst ist Feierabend...

Nächste wichtige Info:

*Diese Klasse Hello ist **eigentlich** nicht wirklich Hello, sondern **dummy.inner.Hello** !!!*

Man nennt diese Angabe der Klasse einschließlich des Paketnamens den *fully qualified* Name der Klasse.

Ok, hier die Info: wir müssen den fully-qualified-name angeben, damit das läuft!

Wir sagen also:

    java dummy.inner.Hello

uuuund bekommen wieder die erste Fehlermeldung!

SCHEISSEEEEE!!! WARUM!?!?!

Geh zurück ins Wurzelverzeichnis wo dieses Info-File liegt und versuchs nochmal!
Sollte jetzt funktionieren!
Think about it; wir haben gesagt:

* die Ordnerstruktur korrespondiert mit den Package-Namen
* wir müssen den fully-qualified Name benutzen

Wir denken weiter:

* irgendwo muss der Anfang gemacht werden, um diesen Namens- und Pfadkram aufzurollen...

Das passiert in dem Ordner, wo wir

    java blablubb

ausführen.
Wenn wir also **innerhalb** von .../dummy/inner

    java dummy.inner.Hello

aufrufen wird versucht **ab dort** diese Unterordnerstruktur zu finden mit der
*.class*-Datei. Und die gibt es halt nicht!
Also müssen wir wieder auf das Wurzelverzeichnis zurück!

