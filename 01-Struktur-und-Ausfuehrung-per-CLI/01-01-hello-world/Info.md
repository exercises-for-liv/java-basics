# Infoooos
Grob: eine einzelne *.java*-Datei beschreibt eine sogenannte *Klasse* (Ausnahmen möglich mit inneren/nested Klassen; erstmal unwichtig).
Das ist die menschenlesbare Variante, die wir in der java-Datei sehen; damit was gemacht werden kann / muss eine solche Datei *kompiliert* werden. (Der Begriff "kompiliert" ist mit leichter Vorsicht zu genießen, weil kein Maschinencode erzeugt wird, aber der Sprachductus ist meist so.)

**REMEMBER: der Dateiname muss mit dem Klassennamen korrespondieren! "Hello.java" -> Klasse muss "Hello" heißen!**

Einzelne Datei kompilieren mit "java-compile":

    javac Hello.java

Der Code wird in *Java-Byte-Code* übersetzt. Der liegt dann in Dateien, die auf
*.class* enden.

Wir können das kompilierte Programm ausführen mit:

    java Hello

Das geht, weil Hello.class eine *main-Methode* enthält. (Damit eine Klasse ausführbar ist MUSS sie eine Main-Methode beinhalten - das ist nicht immer erwünscht: bspw Bibliotheken)
Wir werden uns im nächsten Kapitel mit Methoden beschäftigen und auch sehen,
was es mit der Main-Methode auf sich hat.

Als Vorgeschmack kannst du dir den Inhalt von Hello.java anschauen, der nur
eine Klassendeklaration enthält, die eine einzige Methode enthält (die main-Methode).

**-------------------Bonusinfo---------------------**:
Der Bytecode ist kein echter Maschinencode auch, wenn er ähnlich aussieht, sondern ein Zwischenprodukt, welches von jeder sogenannten *JVM (Java Virtual Machine)* verstanden werden kann, die dann das "heavy-lifting" übernimmt und das ganze so interpretiert, wie es zu dem
jeweiligen Gerät passt...Linux-PC, Windows-PC oder irgendwelche verrückten
embedded Systems uuuund so weiter. 
Das heißt insbesondere, dass man für beliebige Geräte nur eine passende JVM schreiben
muss und schon können (theoretisch) alle Javaprogramme drauf laufen (sofern
alles in der richtigen Version vorliegt :D ). Das war sozusagen der Top-Seller-Punkt
von Java.
Die class-Dateien kann man nicht ohne Weiteres lesbar einsehen, aber man kann 
theoretisch:

    javap -c -p Hello

benutzen, um sich den Byte-Code menschenlesbar reinzuziehen. Dann sieht man,
dass das schon recht nahe an Maschinencode dran ist. Das ist aber alles erstmal
nicht so wichtig und muss absolut nicht verstanden werden zu diesem Zeitpunkt!
**--------------------------------------------------**