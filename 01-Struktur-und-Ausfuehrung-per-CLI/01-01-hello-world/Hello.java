//Eine Klasse mit Namen Hello; sie ist öffentlich (public)...das ist wie mit den Kardashians...jeder der will kann sie sehen, auch, wenn das nicht unbedingt das Beste ist...
public class Hello{
    //The mighty main-Methode! Wenn eine Klasse sowas hat, dann kann sie als Einstiegspunkt zur Programmausführung dienen!
    public static void main(String[] args){
        System.out.println("Hallo da draussen!");
    }
}