# Intro
Wir fangen mit Basics an. Das heißt: erstmal keine Sonderfälle oder Eigenheiten "modernerer" Technologien
wie Modulsystemen oder sonem Gedöns. Das würde nur dazu führen, dass wir uns verzetteln!
Halte nur im Hinterkopf: ein Programmierprojekt in Java besteht nicht zwangsweise
nur aus *.java* oder *.class*-Dateien, wie in diesem Ordner!

Wir beschäftigen uns also damit, wie man einfache Programme über die Command-Line
starten kann und welche Bedingungen strukturell erfüllt sein müssen, damit das 
funktioniert.

## Takeaways

* eine Standard-Datei, die unkompilierten Java-Code enthält, endet auf *.java*; 
  in solche Dateien schreiben wir unseren Code
* pro Datei eine *Klasse* (theoretisch auch innere Klassen und so, soll aber
  angeblich deprecated werden; muss ich nochmal genauer recherchieren)
* kompilierter Java-Code liegt in Dateien mit Endung *.class*
* damit eine *.class* Datei ausgeführt werden kann muss sie eine main-Methode enthalten
* packages wrden mit dem *package* Keyword in den *.java* Dateien deklariert
* die Ordnerstruktur und Platzierung von Dateien muss mit der Packagestruktur korrespondieren
* der *fully-qualified* Name einer Klasse ist *[paket.in.dem.klasse.liegt].[Klassenname]*
* Pass immer auf, in welchem Ordner du CLI-Commands aufrufst. 
  Java ist streng, wenn es um die Aufläsung von Pfaden und so geht!
