package myPackage;

class Audi extends Car {
    Audi(int numberOfWheels, int maxSpeed, String name) {
        super(numberOfWheels, maxSpeed, name);
    }

    void doAudiStuff(){
        System.out.println("I'm an Audi doing stuff");
    }
}
