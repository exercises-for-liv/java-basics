package myPackage;


public class Car {
    int numberOfWheels;
    int maxSpeed;
    String name;

    public Car(int numberOfWheels, int maxSpeed, String name) {
        this.numberOfWheels = numberOfWheels;
        this.maxSpeed = maxSpeed;
        this.name = name;
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }

    public void setNumberOfWheels(int numberOfWheels) {
        this.numberOfWheels = numberOfWheels;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
