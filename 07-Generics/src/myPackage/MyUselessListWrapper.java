package myPackage;

import java.util.ArrayList;
import java.util.List;

//das T nennt man einen "Typparameter"; es dürften mehrere durch Komma getrennte sein!
public class MyUselessListWrapper <T> {
    List<T> theRealList;

    public MyUselessListWrapper(){
        theRealList = new ArrayList<>();
    }

    public boolean addItem(T someItem){
        return theRealList.add(someItem);
    }

    public T getItem(int index){
        return theRealList.get(index);
    }

    public void printList(){
        theRealList.stream()
                .forEach(System.out::println);
    }
}
