package myPackage;

public class Main {
    public static void main(String[] args) {
        MyUselessListWrapper<String> stringListWrapper = new MyUselessListWrapper<>();
        stringListWrapper.addItem("mops");
        stringListWrapper.addItem("schweinchen");
        stringListWrapper.addItem("pupsnase");

        stringListWrapper.printList();

        //durch den generischen Aufbau unserer Klasse wissen wir hier
        //, dass das String ist und müssen nix casten usw. ...cool!
        String someItem = stringListWrapper.getItem(2);
        System.out.println(someItem);

        MyOverlyComplicatedGenericClass<Audi> autos = new MyOverlyComplicatedGenericClass<>();
        autos.setFirstCar(new Audi(5, 20, "Doofes Teil"));
        // autos.setSecondCar(new Mercedes(5, 23, "Anderes doofes Teil")); DAS geht nicht :) ein Mercedes ist kein Audi und das war der Typparameterwert, den wir gesetzt haben
        autos.setSecondCar(new Audi(5, 23, "Anderes doofes Teil"));

        Car[] mehrAutos = new Audi[3]; //DAS geht (arrays sind "kovariant")
        // MyOverlyComplicatedGenericClass<Car> mehrAutos = new MyOverlyComplicatedGenericClass<Audi>(); //DAS geht aber nicht! (generics sind invariant; vermindert die Geführdung der Typsicherheit)

        //was jedoch geht ist folgendes:
        MyOverlyComplicatedGenericClass<?> nochMehrAutos = new MyOverlyComplicatedGenericClass<Audi>();
    }
}
