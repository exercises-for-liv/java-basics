package myPackage;

public class MyOverlyComplicatedGenericClass <T extends Car> {
    T firstCar;
    T secondCar;

    public T getFirstCar() {
        return firstCar;
    }

    public void setFirstCar(T firstCar) {
        this.firstCar = firstCar;
    }

    public T getSecondCar() {
        return secondCar;
    }

    public void setSecondCar(T secondCar) {
        this.secondCar = secondCar;
    }
}
