package myPackage;

public class CuteButUnnecessarilyPoliteException extends Exception {
    public CuteButUnnecessarilyPoliteException(String message){
        super("Don't worry man; nothing serious...just: " + message);
    }
}
