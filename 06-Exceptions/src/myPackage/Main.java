package myPackage;

public class Main {
    public static void main(String[] args) {
        DoMaster masterBlaster = new DoMaster();

        try {
            masterBlaster.throwException();
        } catch (MyBadassException e) {
            e.printStackTrace();
        }

        try {
            masterBlaster.initiateThrowingHell();
        } catch (MyBadassException | CuteButUnnecessarilyPoliteException e) {
            e.printStackTrace();
        }finally {
            //das passiert IMMER!
            System.out.println("I caught a bad guy!");
        }

        //important note: die speziellste Exception-Ableitung sollte immer
        // ganz oben stehen, da der ERSTE matchende catch-block greift!!!
        //Die anderen werden nicht mehr ausgeführt. Sprich deutlich anders
        //als in switch-Konstrukten, wo alles weiterläuft, solange man nicht
        //explizit break sagt.
        //Code der auf jeden fall ausgeführt werden soll nach einem catch,
        //gehört in einen finally-Block.
    }
}