package myPackage;

import java.util.Random;

/**
 * Does things :D
 */
public class DoMaster {
    void throwException() throws MyBadassException{
        throw new MyBadassException("just wanted to show you things going wrong...");
    }

    void initiateThrowingHell() throws MyBadassException, CuteButUnnecessarilyPoliteException {
        int whichOne = new Random().nextInt(2);
        switch (whichOne) {
            case 0:
                //hier sieht man, dass Exceptions über mehrere Ebenen weitergeleitet werden können
                throwException();
                break;
            case 1:
                throw new CuteButUnnecessarilyPoliteException("nothing happened...I just like you and needed an excuse to talk to you. Time this sunday?");
        }
    }
}
