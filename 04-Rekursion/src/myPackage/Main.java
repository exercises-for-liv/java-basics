package myPackage;

class Main{
    public static void main(String[] cliArgumente){
        System.out.println(arithmeticSequenceIterative(10)); // 55
        System.out.println(arithmeticSequenceRecursion(10)); // 55
        System.out.println(fibonacci(7));
        System.out.println(arithmeticSequenceTailRecursion(10, 10)); // 55
        System.out.println(arithmeticSequenceTailRecWrapper(10)); // 55
    }

    //Ansatz: a_n = sum_{i=0}^{n} (i)
    private static int arithmeticSequenceIterative(int maxNumber){
        int result = 0;
        for(int i = 0; i <= maxNumber; i++){
            result = result + i; // 0+1+2+3+4+5+6+7+...+n
        }
        return result;
    }

    //Ansatz: a_n = a_(n-1) + n; a_0 = 0;
    //Vorsicht: sehr naiv; schlechte Lösung!
    private static int arithmeticSequenceRecursion(int maxNumber){
        if(maxNumber == 0){
            return 0;
        }else{
            /*VORSICHT: das ist NICHT endrekursiv (tail-recursion) aufgebaut!!! Sprich: der
            rekursive Funktionsaufruf ist NICHT das letzte, was passiert.
            Endrekursive Funktionen können von einigen moderenen Compilern
            in eine iterative Variante umgebaut werden; das heißt ->
            kein memory overhead!!!

            Hier ist nicht der rekursive Aufruf das letzte, was passiert,
            danach kommt nämlich noch die Addition und die benötigt die vorherige
            Auswertung des rekursiven Aufrufs!!!
            */
            return arithmeticSequenceRecursion(maxNumber - 1) + maxNumber;
        }
    }

    /*Vorsicht: sehr naiv; schlechte Lösung! Die entspricht aber dem, wie man
      typischerweise die Fibonaccifolge kennenlernt F_n = F_(n-1) + F_(n-2) .
      Da ist die rekursive Formulierung recht intuitiv.
      Denk dran: Verankerung nicht vergessen! (die ersten beiden ifs, die
      sicherstellen, dass das ganze irgendwann aufhört :) )
     */
    private static int fibonacci(int n){
        if(n == 0){
            return 0;
        }
        if(n == 1){
            return 1;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
        /* Schlecht, weil wieder keine Endrekursion.
         Im Falle der Fibonacci-Zahlen ist das besonders fatal, da sich zeigen
         lässt, dass die Anzahl der Funktionsaufrufe sehr schnell wächst
         (auch ne Fibonacci-Folge? Müsste ich nochmal durchrechnen...).
         Da das alles nicht endrekursiv ist, kann der Compiler das nicht
         in eine iterative Variante umwandeln und die ganzen Funktionsaufrufe
         brauchen ihre eigenen Stack-Frames. Da die Anzahl der Funktionsaufrufe
         so schnell wächst landet man schon früh im Stack-Overflow.
         */
    }

    /*
     So ginge es mit Endrekursion...ich gebe zu...nicht sehr intuitv.
     Blöderweise gibt es kein perfektes Rezept, um sowas umzuwandeln.
     Das hängt leider vom spezifischen Problem ab.
     Andererseits sind diese Beispiele, wo die Umwandlung NICHT trivial ist
     vermutlich in freier Wildbahn eh deutlich seltener anzutreffen.
     */
    private static int arithmeticSequenceTailRecursion(int n, int accumulator){
        if(n == 0){
            return accumulator;
        }else{
            return arithmeticSequenceTailRecursion(n - 1, accumulator + (n - 1));
        }
    }

    /*
     Die obige Methode ist Kacke was die Nutzerfreundlcihkeit angeht, also
     gibt es hier eine Variante, wo nix schiefgehen kann, indem wir
     vom User die Notwendigkeit wegabstrahieren, dass er wissen muss, was er
     beim Aufruf der Methode für den Accumulator reinwerfen muss.
     Das ist hübscher so...man würde also beim Design einer API die obige Methode
     private lassen und nur diese hier public machen!
     
     Btw.: die Methoden sind alle löchrig!
     Wir haben zum Beispiel nicht drüber nachgedacht, was wir machen,
     wenn eine User sich entscheidet eine Pupsnase zu sein und zB ne negative
     Zahl reinzuwerfen! Um solche Fallstricke zu vermeiden ist es sinnig
     ein Prinzip zu verfolgen, das sich TDD (Test Driven Development) nennt,
     zu dem wir deutlich später kommen werden.
     */
    private static int arithmeticSequenceTailRecWrapper(int n){
        return arithmeticSequenceTailRecursion(n, n);
    }
}