package myPackage;

import java.util.Comparator;
import java.util.List;

public class ReferenceTests {
    public static void main(String[] args) {
        int a = 4;
        int b = 5;
        swap(a,b);

        System.out.println("a: " + a);  // 4
        System.out.println("b: " + b);  // 5


        IntWrap aWrap = new IntWrap(4);
        IntWrap bWrap = new IntWrap(5);
        swapWrap(aWrap, bWrap);

        System.out.println("aWrap.value: " + aWrap.value);  // 4
        System.out.println("bWrap.value: " + bWrap.value);  // 5

        //ABER (hier ist ja nix gecalled worden):

        IntWrap temp = aWrap;
        aWrap = bWrap;
        bWrap = temp;

        System.out.println("aWrap.value: " + aWrap.value);  // 5
        System.out.println("bWrap.value: " + bWrap.value);  // 4


        //ABER:

        doSomething(bWrap);
        System.out.println(bWrap.value);  // 7
    }

    private static void swap(int a, int b) {
        int temp = a;
        a = b;
        b = temp;
    }

    private static void doSomething(IntWrap wrap) {
        wrap.value = 7;
    }

    private static void swapWrap(IntWrap a, IntWrap b) {
        IntWrap temp = a;
        a = b;
        b = temp;
    }
}
