package myPackage;

public class Example {
    public static void main(String[] args) {
        MyComplexData data1 = new MyComplexData(1, "Hallo");
        MyComplexData data1imposter = new MyComplexData(1, "Hallo");
        MyComplexData data2 = new MyComplexData(2, "Hallo");
        MyComplexData data3 = new MyComplexData(3, "Hallo");
        MyComplexData data4 = data3;

        System.out.println("----------------");
        System.out.println(data1);
        System.out.println(data2);
        System.out.println(data3);
        System.out.println(data4);

        System.out.println(sameReference(data1, data1imposter));

        System.out.println("----------------");
        System.out.println(data3.dataception);
        System.out.println(data4.dataception);
        System.out.println(sameReference(data4.dataception, data3.dataception));

    }

    static boolean sameReference(Object firstThing, Object secondThing){
        if (firstThing == secondThing) {
            return true;
        }else{
            return false;
        }
    }
}
