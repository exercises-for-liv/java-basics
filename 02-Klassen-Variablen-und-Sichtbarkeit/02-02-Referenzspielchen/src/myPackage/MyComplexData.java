package myPackage;

class MyComplexData {
    int zahl;
    String text;

    MyComplexData2 dataception;

    public MyComplexData(int zahl, String text) {
        this.text = text;
        this.zahl = zahl;
        this.dataception = new MyComplexData2(42);
    }
}
