### Takeaways

* Scope: (auch) Blockbereiche, in denen Variablen sichtbar sind
    * denk an die Maislabyrinthe auf verchiedenen Ebenen
    * von höher kann man die Variablen sehen, die weiter unten deklariert wurden
    * in die gleiche Ebene kann man nicht reinschauen; Bsp: Geilomatiko->nochNeZahl und doSomething
* Tabelle mit Sichtbarkeitsmodifikatoren: https://docs.oracle.com/javase/tutorial/java/javaOO/accesscontrol.html
- private: etwas kann nur von innerhalb derSELBEN Klasse gesehen werden
- private package: ist default; etwas kann nur von innerhalb desselben Packages gesehen werden    (darf in anderer Klasse sein)
- protected: private package + etwas kann gesehen werden von einem Kind, auch wenn es nicht im     selben package ist (a in package y kann gesehen werden von b in package z, wenn:                 b extends a)
- public: kann von überall aus gesehen werden (ist im Regefall zu vermeiden - ggfs                 Sicherheitsrisiko)
* Tabelle mit Einsatzmöglichkeiten von Sichtbarkeitsmodifikatoren: http://tutorials.jenkov.com/java/access-modifiers.html
* Java kennt nicht das Konzept von Subpackages auf einer semantischen Ebene!!!
    * beachte das beim Modifikator "package private" (also: "nix hinschreiben")
* In Java ist die Beziehung zwischen Klassen im gleichen Paket konzeptionell
  eine engere als zwischen einer Klasse und ihren Ableitungen; **think about it!**
