package myPackage.inner;

class UnreachableStar{
    //Da die Klasse schon nicht public ist, kann ich die Variable obwohl sie 
    //public ist nicht in einem anderen package sehen.
    //Wir sehen die Truhe nicht, also den Inhalt erst recht nicht!
    public int zahl;
}