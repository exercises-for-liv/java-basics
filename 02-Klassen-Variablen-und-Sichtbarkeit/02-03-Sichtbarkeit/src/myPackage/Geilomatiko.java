package myPackage;

class Geilomatiko{
    int zahl;
    //das ist mein Geheimnis! Nur meine Homies können die geheime Zahl sehen!
    private int geheimeZahl;

    public Geilomatiko(int zahl){
        this.zahl = zahl;
        this.geheimeZahl = 42;
        int nochNeZahl = 111;
        doSomething();
        this.geheimeZahl = 31;
        doSomething();
    }


    private void doSomething(){
        System.out.println("I do stuff and I know a secret!");
        System.out.println(this.geheimeZahl);
        //System.out.println(nochNeZahl); geht nicht, weil nochNeZahl nicht in
        //dem "Scope" von doSomething
    }
}