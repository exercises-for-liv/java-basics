package myPackage;

//liegt in anderem Package ist aber public, ich kanns also sehen und zu mir 
//einladen :) und das macht es gerne, weils ein Exhibitionist ist!
import myPackage.inner.Exhibitionist;
import myPackage.inner.innerer.ExhibitionistAdvanced;

//import myPackage.inner.UnreachableStar;  funktioniert hier NICHT, weil 
//UnreachableStar in einem anderen Package liegt und die Klasse package-private ist!!!
//Wichtiges Takeaway: Java kennt nicht das Konzept von Subpackages auf einer 
//semantischen Ebene!!!

class Main{
    public static void main(String[] args){
        //Geilomatiko liegt im gleichen Package und das Feld "zahl" ist 
        //ebenfalls auf package private gestellt, kann also eingesehen und modifiziert werden
        Geilomatiko geilomat = new Geilomatiko(42);
        geilomat.zahl = 12;

        System.out.println("Geänderte package private zahl in geilomat:");
        System.out.println(geilomat.zahl);

        //darf ich...das ist consensual, er steht drauf...
        new Exhibitionist();

        //ist auch public
        new ExhibitionistAdvanced();
    }
}