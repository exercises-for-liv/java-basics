### Takeaways
* es gibt *primitive* und *komplexe* Datentypen in Java
* *primitive* Datentypen werden dir "frei Haus" geliefert; hauptsächlich verschiedene Zahltypen (https://www.baeldung.com/java-primitive-conversions)
* *komplexe* Datentypen sind aus einer oder mehreren primitiven oder weiteren komplexen Daten zusammengesetzte Konstrukte
* diese Konstrukte nennen wir *Klassen*
* in Java besteht eine minimale Klassendeklaration aus dem Keyword *class* + *Klassenname* + {}
    * der Klassenname **MUSS** gleich dem Dateinamen sein!!!!!!!!!!!!!!!!!!!
* eine Klasse ist wie ein Bauplan; mit diesem Bauplan können wir *Objekte* erzeugen (man sagt *instanzieren*; daher auch manchmal *Instanz* statt Objekt)
-> grundsätzlich gilt aber, dass eine Klasse einen Zustand hat (nämlich die beschreibenden Variablen) und ein Verhalten (ihre Methoden)
* für jeden primitiven Datentyp stellt Java einen "komplexen" großen Bruder bereit (Bsp.: *Integer* für *int*)
* für die Interoperabilität von Primitivum und großem Bruder gelten etwas besondere Regeln, die sonst nicht gelten
    * lies das: https://javabeginners.de/Grundlagen/Datentypen/Boxing_und_Unboxing.php
* *das was für ein komplexes Datum in einer Variable gespeichert wird ist ein Referenzwert, also ein "Wegweiser" wo das Objekt im Speicher zu finden ist*
* *== vergleicht primitive Werte tatsächlich am Wert* 
* *== vergleicht komplexe Werte anhand des Referenzwertes* (ist es DAS SELBE; da wird also auf den Wert der Referenz also quasi den Ort im Speicher getestet und nicht ob die Innereien gleich sind)
* weitere Erläuterung: Java ist *Call by Value*, was allerdings verwirrend werden kann siehe in 02-02 die Klasse ReferenzTests und die entsprechenden Anmerkungen für mehr
