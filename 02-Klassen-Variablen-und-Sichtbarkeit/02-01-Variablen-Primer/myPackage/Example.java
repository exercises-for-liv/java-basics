package myPackage;

class Example {

    public static void main(String[] args) {
        int zahl = 10;
        byte byteZahl = 10;

        Integer gewrappteZahl = new Integer(10);
        Integer gewrappteZahl2 = new Integer(10);

        if (zahl == byteZahl) {
            System.out.println("zahl und bytezahl gleich!");
        }

        //das ist tatsächlich so, weil gewrappteZahl und gewrappteZahl2 an unterschiedlichen Punkten im Speicher liegen
        if (gewrappteZahl != gewrappteZahl2) {
            System.out.println("gewrappte Zahl nicht gleich gewrappte zahl 2");
        }

        // zugegeben leicht verwirrend; hier wird primitiv gegen komplex verglichen. 
        // Da schafft Java es die korrekten Schritte zu unternehmen, damit der Wert verglichen wird
        if (gewrappteZahl == zahl) {
            System.out.println("gewrappte Zahl gleich zahl");
        }

        // ABER new Integer(...) ist deprecated (soll man nicht mehr nutzen heißt das)
        // und die neuen Methoden machen uns das Leben etwas einfacher mit Zahlen umzugehen
        // so kommt bei Zahlvergleichen Tatsache raus, was ich erwarten würde
        Integer uno = Integer.valueOf(11);
        Integer dos = Integer.valueOf(11);

        if (uno == dos) {
            System.out.println("Woop woop uno und dos sind gleich!");
        }

        // but Vorsicht: das ist ein SONDERVERHALTEN und nicht der "Normalzustand"
        // der Normalzustand ist der, wie er in Zeilen 16-19 zu sehen ist!
        // Das liegt daran, dass für komplexe Typen der Vergleichspunkt normalerweise
        // die Referenz auf den Platz im Speicher ist!
    }
}