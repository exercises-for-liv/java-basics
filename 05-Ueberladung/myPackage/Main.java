package myPackage;

class Main{
    public static void main(String[] args){
        doSomething();
        doSomething("hot shit!");
    }

    private static void doSomething(){
        System.out.println("I am doing something!");
    }

    //damit Methoden "korrekt" überladen sind, müssen sie unterschiedliche Parameterlisten haben
    //oder anders formuliert: Überladungen, die nur unterschiedliche Rückgabetypen haben, krachen!
    //Aufgabe: think about it!
    private static void doSomething(String stuff){
        System.out.println("I'm doing something else, namely: " + stuff);
    }

    /*
      Not so funny fact: Java sieht leider keine Operatorüberladung vor :(
      Operatoren sind +, - uuuund noch ne Menge mehr: siehe Vorlesung!
      Das heißt, dass zum Beispiel folgendes NICHT geht:

      public static Vector +(Vector vector1, Vector vector2){
          Vector vectorAddition = new Vector(vector1.length);
          for(int i = 0; i < vector1.length; i++){
              vectorAddition.set(i, vector1.get(i) + vector2.get(i));
          }
          return vectorAddition;
      }

      [...]

      Vector resultVector = someVector + someOtherVector;

      ANSTATT 
      Vector resultVector = Vector.addVectors(someVector, someOtherVector);
     */

}