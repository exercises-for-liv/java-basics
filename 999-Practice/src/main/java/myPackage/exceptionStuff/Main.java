package myPackage.exceptionStuff;

public class Main {
    public static void main(String[] args) {
        doWrapper();
    }

    private static void doSomething() throws IllegalArgumentException{
        throw new IllegalArgumentException("There are no arguments");
    }

    private static void doWrapper() {
        try {
            doSomething();
        } catch (IllegalArgumentException e) { //Kannste fangen...musste nit...sollteste vermutlich auch nicht, da ja ne checked exception tendenziell auf nen Programmierfehler hinweisen sollte
            e.printStackTrace();
        }
    }
}
