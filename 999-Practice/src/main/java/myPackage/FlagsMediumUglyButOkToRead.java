package myPackage;

class FlagsMediumUglyButOkToRead {
    public static void main(String[] args) {
        schweiz(3);
    }

    static void schweiz(int n){
        int maxRow = 3 * n;
        int maxColumn = 6 * n + 3;

         for (int row = 0; row < maxRow; row++){
            StringBuilder builder = new StringBuilder();
            for (int column = 0; column < maxColumn; column++) {
                if (
                        isInVerticalThing(n, row, column)
                        || isInHorizontalThing(n, row, column)
                ) {
                    builder.append("s");
                }else{
                    builder.append("O");
                }
            }
            System.out.println(builder.toString());
        }
    }

    private static boolean isInVerticalThing(int n, int currentLine, int currentColumn){
        int maxColumn = 6 * n + 3;
        int beginLineVertical = 1;
        int endLineVertical = 3 * n - 2;
        int beginColumnVertical = maxColumn / 2 - 1;
        int endColumnVertical = maxColumn / 2 + 1;

        return currentLine >= beginLineVertical
                && currentLine <= endLineVertical
                && currentColumn >= beginColumnVertical
                && currentColumn <= endColumnVertical;
    }

    private static boolean isInHorizontalThing(int n, int currentLine, int currentColumn) {
        int maxColumn = 6 * n + 3;

        int beginLineHorizontal = n ;
        int endLineHorizontal = 2 * n - 1;
        int beginColumnHorizontal = n;
        int endColumnHorizontal = maxColumn - n - 1;

        return currentLine >= beginLineHorizontal
                && currentColumn >= beginColumnHorizontal
                && currentLine <= endLineHorizontal
                && currentColumn <= endColumnHorizontal;
    }
}
