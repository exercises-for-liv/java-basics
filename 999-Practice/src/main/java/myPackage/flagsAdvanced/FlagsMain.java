package myPackage.flagsAdvanced;

public class FlagsMain {
    public static void main(String[] args) {
        SwizFlagPainter flagPainter = new SwizFlagPainter(
                new VerticalBarTester(),
                new HorizontalBarTester()
        );
        flagPainter.paintFlag(3);

        new SimpleFlagPainter().paintFlag(5);
    }
}
