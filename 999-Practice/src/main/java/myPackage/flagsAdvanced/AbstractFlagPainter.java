package myPackage.flagsAdvanced;

public abstract class AbstractFlagPainter implements FlagPainter{
    private final FlagCharCalculator charCalculator;

    protected AbstractFlagPainter(FlagCharCalculator charCalculator) {
        this.charCalculator = charCalculator;
    }

    @Override
    public void paintFlag(int n) {
        int maxRow = getMaxRow(n);
        int maxColumn = getMaxColumn(n);

        for (int row = 0; row < maxRow; row++){
            StringBuilder builder = new StringBuilder();
            for (int column = 0; column < maxColumn; column++) {
                builder.append(charCalculator.calculateChar(row, column));
            }
            System.out.println(builder.toString());
        }
    }

    private int getMaxRow(int n) {
        return 3 * n;
    }

    private int getMaxColumn(int n){
        return 6 * n + 3;
    }
}
