package myPackage.flagsAdvanced;

public class HorizontalBarTester {

    boolean isInHorizontalThing(int n, int currentLine, int currentColumn) {
        int maxColumn = 6 * n + 3;

        int endLineHorizontal = 2 * n - 1;
        int endColumnHorizontal = maxColumn - n - 1;

        return currentLine >= n
                && currentColumn >= n
                && currentLine <= endLineHorizontal
                && currentColumn <= endColumnHorizontal;
    }
}
