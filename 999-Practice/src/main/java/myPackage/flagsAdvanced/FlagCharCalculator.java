package myPackage.flagsAdvanced;

@FunctionalInterface
public interface FlagCharCalculator {
    String calculateChar(int rowIndex, int columnIndex);
}
