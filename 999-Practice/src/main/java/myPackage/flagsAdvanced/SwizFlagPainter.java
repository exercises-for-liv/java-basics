package myPackage.flagsAdvanced;

public class SwizFlagPainter implements FlagPainter{
    private VerticalBarTester vertTester;
    private HorizontalBarTester horiTester;

    public SwizFlagPainter(
            VerticalBarTester vertTester,
            HorizontalBarTester horiTester) {
        this.vertTester = vertTester;
        this.horiTester = horiTester;
    }

    @Override
    public void paintFlag(int n) {
        int maxRow = getMaxRow(n);
        int maxColumn = getMaxColumn(n);

        for (int row = 0; row < maxRow; row++){
            StringBuilder builder = new StringBuilder();
            for (int column = 0; column < maxColumn; column++) {
                if (
                    vertTester.isInVerticalThing(n, row, column)
                    || horiTester.isInHorizontalThing(n, row, column)
                ) {
                    builder.append("s");
                }else{
                    builder.append("O");
                }
            }
            System.out.println(builder.toString());
        }
    }

    private int getMaxRow(int n) {
        return 3 * n;
    }

    private int getMaxColumn(int n){
        return 6 * n + 3;
    }
}
