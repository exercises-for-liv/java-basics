package myPackage.flagsAdvanced;

public class VerticalBarTester {
    boolean isInVerticalThing(int n, int currentLine, int currentColumn){
        int maxColumn = 6 * n + 3;
        int beginLineVertical = 1;
        int endLineVertical = 3 * n - 2;
        int beginColumnVertical = maxColumn / 2 - 1;
        int endColumnVertical = maxColumn / 2 + 1;

        return currentLine >= beginLineVertical
                && currentLine <= endLineVertical
                && currentColumn >= beginColumnVertical
                && currentColumn <= endColumnVertical;
    }
}
