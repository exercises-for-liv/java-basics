package myPackage.flagsAdvanced;

public interface FlagPainter {
    void paintFlag(int n);
}
