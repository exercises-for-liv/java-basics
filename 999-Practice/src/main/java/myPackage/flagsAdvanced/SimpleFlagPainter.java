package myPackage.flagsAdvanced;

public class SimpleFlagPainter extends AbstractFlagPainter {
    protected SimpleFlagPainter() {
        super(new SimpleFlagCharCalculator());
    }
}
