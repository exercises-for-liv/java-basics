## Takeaways

* geerbt bzw. erweitert wird durch das keyword *extends*
* man kann immer nur eine Klasse erweitern, Mehrfachvererbung ist böse wegen möglicher Komplikation durch die Polymorphie!!!
* man kann auf Variablen, Methoden und den Konstruktor der "Vaterklasse" mit dem keyword *super* zugreifen
* Methoden, die nicht überschrieben werden, werden automatisch von der "Vaterklasse" genommen, müssen also eine dementsprechende Signatur aufweisen, denn wenn es diese Methode mit dieser Signatur dort nicht gibt fliegt einem das Ding um die Ohren