package myPackage;

class JobJob extends Job{

    private int workHours;

    /*Folgendes wird nicht gehen:
    Job(){}
    WEIL
        in diesem Konstruktor automatisch bei der "Weiterreichung" des 
        Konstruktionsvorganges versucht würde den default-Konstruktor der 
        Oberklasse aufzurufen. Der existiert nicht, da wir selbst einen
        spezifiziert haben.
        Da dieser ein Argument (int) erwartet müssen wir den explizit auf-
        rufen. Siehe unten:
     */

    JobJob(){
        //super innerhalb eines Konstruktors ruft den Konstruktor der 
        // Oberklasse auf! Das MUSS das erste sein, was hier passiert!
        super(42);
        System.out.println("MOPETH");
        this.workHours = 80;
    }

    @Override
    void doSomething(){
        System.out.println("I'm a JobJob! You can do me way worse than a normal job! I'm twice as much job as a normal job...");
    }

    void doSomethingSuper(){
        super.doSomething();
    }

    void tellMeMyWorkhours(){
        System.out.println(this.workHours);
        System.out.println(super.workHours);
    }

    //Hausaufgabe; lies folgendes ("Shadowing"): https://dzone.com/articles/variable-shadowing-and-hiding-in-java
}