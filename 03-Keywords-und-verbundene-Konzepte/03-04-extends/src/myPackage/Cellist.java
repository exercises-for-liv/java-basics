package myPackage;

class Cellist extends Musician{
    
    @Override
    void makeMusic(String piece){
        String gloriousPiece;
        if (piece == null || piece.equals("")) {
            gloriousPiece = defaultPiece; // defaultPiece kommt aus der Vaterklasse
        }else{
            gloriousPiece = piece;
        }
        System.out.println("I'm a cellist and today I will play for you: " + gloriousPiece);
    }
}