package myPackage;

class Job{
    //streng genommen würde hier package private reichen
    protected int workHours;

    //Anatomie eines Constructors: [Sichtbarkeitsmodifikator] [Klassenname] ([Parameter]) { [Body] }
    Job(int workHours){
        this.workHours = workHours;
    }

    void doSomething(){
        System.out.println("I'm a job! You can do me!");
    }
}