package myPackage;

class Main{
    public static void main(String[] args){
        // Der Musiker-Teil
        Musician cellist = new Cellist();
        Musician saxophonist = new Saxophonist();

        cellist.makeMusic("Brahms Sonate, Dm"); // I'm a cellist and today I will play for you: Brahms Sonate, Dm
        saxophonist.makeMusic("Take...7.5"); // I'm a saxophonist and today I will blow you away with: Take...7.5

        cellist.makeMusic(null); // I'm a cellist and today I will play for you: Canon in D
        saxophonist.makeMusic(""); // I'm a saxophonist and today I will blow you away with: in the mood
        // Ende des Musiker-Teils


        // Der ... gute Frage...der Job-Teil!
        Job job = new JobJob();

        job.doSomething(); // I'm a JobJob! You can do me way worse...
        // job.doSomethingSuper(); gibt nen Compilezeit-Fehler, weil in "Job" kein doSomethingSuper definiert ist.

        // Was aber geht ist folgendes:
        JobJob jobjob = new JobJob();
        jobjob.doSomethingSuper();

        // super mit Variablen
        jobjob.tellMeMyWorkhours(); // 80  LineBreak  42
    }
}