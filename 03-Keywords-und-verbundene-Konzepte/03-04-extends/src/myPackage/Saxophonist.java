package myPackage;

class Saxophonist extends Musician{
    @Override
    void makeMusic(String piece){
        String gloriousPiece = "in the mood";
        if(!(piece.equals("") || piece == null)){
            gloriousPiece = piece;
        }
        System.out.println("I'm a saxophonist and today I will blow you away with: " + gloriousPiece);
    }
}