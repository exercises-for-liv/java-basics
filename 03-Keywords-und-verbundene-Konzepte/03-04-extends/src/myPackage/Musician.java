package myPackage;

class Musician{
    String defaultPiece = "Canon in D";

    void makeMusic(String piece){
        System.out.println("I'm a musician and today I will present you the famous piece: " + piece);
    }
}