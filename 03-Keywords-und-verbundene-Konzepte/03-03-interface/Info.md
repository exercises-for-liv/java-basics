## Takeaways

* eine Klasse kann ein oder mehrere *interfaces* "implementieren" (keyword: *implements*)(im Gegensatz zu "extends")
* ein interface enthält insbesondere Methodensignaturen ohne Body (alles Weitere in 
  einer eigenen Lessom)
* jede interface-methode ist automatisch public (außer sie besitzt eine default-implementierung)
* das interface selbst darf public oder package-private sein
* eine Klasse (die nicht abstract ist) und ein interface "implementiert" 
  verpflichtet sich dazu diese Methoden zu implementieren, ihnen also einen 
  body zu geben sozusagen :D
* ein interface definiert also sozusagen einen Vertrag, den die implementierende
  Klasse verspricht einzuhalten
* wir dürfen als Typ für Variablen ein interface angeben
* dadurch können wir erreichen (denk darüber nach!!!):
    * bessere Austauschbarkeit (auch zur Laufzeit)
    * höhere Flexibilität bei der Erweiterung (extensibility; nicht im Sinne des 
      Keywords "extends")
    * in Zusammenhang mit extensibility -> höhere maintainability
    * Pseudo-Mehrfachvererbung


Was uns noch fehlt:
* Variablen in Interfaces
* default-Methoden in Interfaces
* @FunctionalInterface - interfaces (das dauert noch bisschen :D ist aber cool!)