package myPackage.kitchen;

//REMARK eine Klasse darf theoretisch mehrere interfaces impementieren (kommasepariert anfügen)!
//AUFGABE: mach dir nochmal deutlich, warum dann das Klassendiamant-Problem nicht auftritt! Was ist das eigentlich!?
class Curry implements Cookable{
    //Wenn ich diese Klasse absolut leer lasse, dann ist der Compiler not amused und sagt mir folgendes:
    // Error:(3, 1) java: myPackage.kitchen.Curry is not abstract and does not 
    // override abstract method cookMe() in myPackage.kitchen.Cookable

    @Override
    public String cookMe(){
        return "Fry chicken, onion and stuff and get down with me in a bed of rice you hottie!";
    }
}