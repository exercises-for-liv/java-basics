package myPackage.kitchen;

import java.util.ArrayList;
import java.util.List;

class Cook{
    List<Cookable> currentMealsToPrepare;

    public Cook(){
        this.currentMealsToPrepare = new ArrayList<>();
    }

    public void addMealToPreparationList(Cookable cookable){
        this.currentMealsToPrepare.add(cookable);
    }

    //THAT'S where the magic happens! Den Cook interessiert es nicht, wie das geht
    // das hat das Gericht (das Cookable) selbst zu wissen!
    // Vorteil: wenn jemand ein neues Gericht erfindet, implementiert er Cookable und
    // ich muss an MEINEM code exakt NULL Zeilen ändern. Arbeit gespart! Geil!
    // Weiterhin muss ich nicht dauernd meine Eingaben auf ihren Typ testen!
    // Solange die mir mittels Cookable versichern, dass man sie kochen kann
    // ist mir der Rest voll egal!
    public String prepareMeal(Cookable cookable){
        return cookable.cookMe();
    }

    //nur ein Beispiel, was man noch machen könnte
    public void prepareAllMeals(){
        for (int i = 0; i < currentMealsToPrepare.size(); i++) {
            Cookable cookable = currentMealsToPrepare.get(i);
            cookable.cookMe();
        }
    }
}