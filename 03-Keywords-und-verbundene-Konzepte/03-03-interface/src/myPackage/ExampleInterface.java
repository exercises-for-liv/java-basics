package myPackage;

//Welcome to: anatomy of an interface - basics
/*
    1. Keyword: interface
    2. Name des Interfaces
    3. body

    Ansonsten gilt wie für classes: Name der Datei beachten, *.java, package beachten.
 */
interface ExampleInterface{
    /*Das klassische interface enthält KEINE Implementierungen 
    (das stimmt inzwischen nicht mehr ganz, weil es inzwischen 
    "default"-Methoden gibt, die auch im interface eine Implementierung erhalten).
    Aaaaber darum geht's uns (noch) nicht.
    Ein interface enthält vor allem in den meisten Fällen Signaturen von Methoden.
    Look at this Beispiel:
     */

    public void doSomething(String someInformation); //<- you see...da kommt nix mehr

    /*
        Watt soll dat?
        Was mach ich mit ner Menge Methodendefinitionen ohne body?
        Siehe dazu das Koch-Beispiel, welches auch weitere nötige syntaktische
        Sachen veranschaulicht.
     */
}