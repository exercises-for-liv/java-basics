package myPackage;

class Wiederholung{
    /*Remember:
            * Variablendeklarationen haben einen bestimmten Aufbau
            * abstrakt: [Sichtbarkeitsmodifikator(+ andere Keyword-Modifikatoren)] [Typ] [Name] -> optional noch ne Zuweisung
     */

    String value = "Hallo"; //wichtig: es darf alles in value rein, was den Typ String hat

    //Pass uff: das heißt auch alles, was von String abgeleitet ist!!!
    /*
        Beispiel:
        Du hast ne Klasse "class MyAugmentedString extends String{...}",
        dann dürfte auch eine Instanz dieser Klasse der Variable value
        zugewiesen werden.
     */

     // WICHTIG: der Typ einer Variable bestimmt sozusagen, was der Compiler erwartet, dass das
     // dort eingesetzte Objekt können muss!
}