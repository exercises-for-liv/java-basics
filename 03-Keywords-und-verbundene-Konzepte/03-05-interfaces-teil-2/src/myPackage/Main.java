package myPackage;

class Main{
    public static void main(String[] args) {
        //Behold: viele Wege führen zur Nutzung eines Interfaces

        //###### 1. "echte" Klasse implementiert Interface, wir benutzen Klasse ######
        Speaker loudSpeaker = new LoudSpeaker();
        letTheSpeakerSpeak(loudSpeaker, "ihr seid pisser!"); // IHR SEID PISSER!

        //###### 2. "anonyme" Klasse implementiert Interface ######
        letTheSpeakerSpeak(
            new Speaker(){
                @Override
                public void speak(String message) {
                    System.out.println(message + ", wenn ich mir diese Bemerkung erlauben darf.");
                } // hier drin würde "this" auf die erzeugte anonyme Instanz zeigen
            }
            , "ich denke xy"
        ); // ich denke xy, wenn ich mir diese Bemerkung erlauben darf.

        //###### 3. per Lambda-Ausdruck ######
        /*
            Nötiges Vorwissen:
                ein Lambda-Ausdruck darf überall dort stehen, wo der erwartete Typ ein 
                sogenanntes "funktionales Interface" ist.
                Das heißt: 
                    Ein Interface mit EXAKT einer abstrakten/nicht-default-Methode!
                    (sprich genau eine Methode ohne Body)
         */
        letTheSpeakerSpeak(
            (text) -> { System.out.println("Die Logik impliziert folgendes: " + text);} // hier drin würde this auf die äußere Instanz zeigen, sprich Main (zumindest war das in Java 9 glaube ich so) SPRICH: Lamda-Ausdrücke sind NICHT NUR syntaktischer Zucker
            , "ich denke QZ ist hier richtig."
        ); // Die Logik impliziert folgendes: ich denke QZ ist hier richtig.

        //###### 4. per Method-Reference ######
        /*
            Think this: Lambda-Ausdruck on steroids
            Siehe auch: https://www.codementor.io/eh3rrera/using-java-8-method-reference-du10866vx
         */
        letTheSpeakerSpeak(
            System.out::println, // Kurzschreibweise für: (text) -> { System.out.println(text); } // Das geht also, wenn der Lambda-Ausdruck, der hier stehen könnte nur eine Methode aufrufen würde. Die muss natürlich zur Signatur von Speaker.speak passen
            "Das ist ein normaler Text."
        ); // Das ist ein normaler Text.
    }

    private static void letTheSpeakerSpeak(Speaker speaker, String message) {
        speaker.speak(message);
    }
}