package myPackage;

@FunctionalInterface
interface Speaker{
    void speak(String message);
}