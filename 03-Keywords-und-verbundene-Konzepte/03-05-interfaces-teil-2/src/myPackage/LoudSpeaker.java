package myPackage;

class LoudSpeaker implements Speaker{
    @Override
    public void speak(String message){
        System.out.println(message.toUpperCase());
    }
}