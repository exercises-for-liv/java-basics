package myPackage;

class UseStaticFunction{
    public static void main(String[] args){

        // weil die Methode "addTwo" static ist, darf ich die einfach auf der Klasse aufrufen,
        // ohne, dass ich eine konkrete Instanz der Klasse benötige
        System.out.println(
                StaticExample.addTwo(2)
        ); // 4

        StaticExample example1 = new StaticExample();
        StaticExample example2 = new StaticExample();

        example1.instanzZahl = 4;
        example2.instanzZahl = 5;

        System.out.println(example1.instanzZahl); // 4
        System.out.println(example2.instanzZahl); // 5

        //ABER:

        example1.klassenZahl = 4;
        example2.klassenZahl = 5;

        System.out.println(example1.klassenZahl); // 5
        System.out.println(example2.klassenZahl); // 5
    }
}