package myPackage;

class StaticExample{
    // jede konkrete Instanz der Klasse hat ihre eigene
    int instanzZahl;
    // static Variablen werden von ALLEN Instanzen der Klasse geteilt (das ist DIESELBE)
    static int klassenZahl;

    public static int addTwo(int value){
        return value + 2;
    }
}