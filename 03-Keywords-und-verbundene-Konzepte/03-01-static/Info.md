## Takeaways

* Variablen und Funktionen, die *static* sind werden von ALLEN Instanzen der 
  entsprechenden Klasse geteilt, d.h. die benutzen DIESELBEN. Es ist nicht
  so, dass jede Instanz seine eigenen Versionen hat. 
  -> Klasse Fahrrad hat static-Variable int Radanzahl = 2, bedeutet jede Instanz eines Fahrrades hat 2 Räder, bedeutet aber auch, dass wenn diese Variable irgendwo auf 1 gesetzt wird, jedes Fahrrad nur noch 1 Rad hat!
  ! nicht mit final verwechseln!
* sie sind nutzbar, **OHNE**, dass auch nur eine einzige Instanz der Klasse existiert
    * Aufruf *[Klassenname].[Variablenname/Methodenname]* (sofern die im entsprechenden 
      Sichtbarkeitsbereich liegen)
* *public static final* oft genutzt zur Konstantendeklaration (Bsp.: Integer.MAX_VALUE)
-> ist nicht nur static, sondern kann auch nicht mehr geändert werden, final eben => good practice: alles in Upper case
* *static* Funktionen oft in Utility-Klassen ( Bsp.: Math.sqrt(...) ) oder als Factory-Methoden