package myPackage;

//let's talk about loops! Die ikonischste als Erstes!

/*
       __                       _
     / _|                     | |
    | |_ ___  _ __   ______  | | ___   ___  _ __  ___
   |  _/ _ \| '__| |______| | |/ _ \ / _ \| '_ \/ __|
  | || (_) | |             | | (_) | (_) | |_) \__ \
 |_| \___/|_|             |_|\___/ \___/| .__/|___/
                                        | |
                                       |_|
 */

 //Ziel: Wiederholungskram!
public class Main {
    public static void main(String[] args) {
        for(int i = 0; i < 3; i++){
            System.out.println("Current index: " + i); 
        }

        int j = 0;
        for(;j < 3; j++){
            System.out.println("Current value of j: " + j);
        }

        int k = 0;
        boolean imTheConstraint = true;
        for(;imTheConstraint;){
            System.out.println("Current value of k: " + k);
            k++;
            if(k == 3){
                imTheConstraint = false;
            }
        }

        int l = 0;
        for(;;){
            System.out.println("Current value of l: " + l);
            l++;
            if(l == 3){
                break; //bricht die Schleife ab: SOFORT; 
                       //statements, die INNERHALB der Schleife NACH "break" stehen, werden nicht mehr ausgeführt
            }
        }

        //Das waren jetzt lauter Arten die for Schleife anzustoßen und auch wieder abzubrechen/zu vollenden
        //Zuletzte noch ein etwas anderes Konzept: "überspringen"
        //Das geht mit "continue". Funktioniert wie break, ABER bricht die Schleife nicht ab, sprich:
        //der Rest der Schleife wird nicht mehr ausgeführt und wir starten direkt von oben.
        //MERKE: das i++ wird TROTZDEM ausgeführt!
        for(int i = 0; i < 3; i++){
            if(i == 1){
                continue;
            }
            System.out.println("Look, the index is: " + i);
        }
    }
}